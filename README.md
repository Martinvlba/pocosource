LineageOS (PocoCustom)
===========

Getting started
---------------
##### Note: Includes fully beryllium device tree with vendor, vince needs vendor stuff and this repo dosent include nethunter stuff needs to be added manually.
##### This repo is just for me easyer to get things ready if needed to move.
##### Alos sorry for bad english if present.
To get started with Android/LineageOS, you'll need to get
familiar with [Repo](https://source.android.com/source/using-repo.html) and [Version Control with Git](https://source.android.com/source/version-control.html).

To initialize your local repository using the LineageOS trees, use a command like this:
```
repo init -u https://gitlab.com/Martinvlba/pocosource.git -b master
```
Then to sync up:
```
repo sync
```
Please see the [LineageOS Wiki](https://wiki.lineageos.org/) for building instructions, by device.
